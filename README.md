# README

Este repo tiene como propósito tener un entorno preparado para trabajar:

* En PHP `7.2`
* Con MySQL `5.7`
* Xdebug en remoto por el puerto `9000`

## Requerimientos

* Docker desktop community en Mac, versión `2.2.0.0` o superior

## Empezando...

El `WEB_DOCUMENT_ROOT` apunta a la carpeta `public` de la raiz de este repositorio. Se podrá acceder al web server por el puerto `8080` y la base de datos por el puerto `3350`.

Para arrancar el Web Server y la Base de datos ejecutar el comando:

```makefile
make run-server
```

A continuación podrás acceder a `http://localhost:8080` que servirá el script `./public/index.php`

## Opciones disponibles

Ejecuta el comando `make` o `make help` en la raiz de este repositorio y obtendrás la siguente salida:

```bash
% make help
-------------------------------------------------
Entorno de desarrollo local para PHP ;) 
-------------------------------------------------
usage: make [target]

targets:
help                      Show this help message
run-server                Start the deployments containers (Detached)
run-server-atached        Start the deployments containers (Atached mode)
stop-server               Stop the deployments containers
enter-apache              ssh's into the be container
enter-db                  ssh's into the be container
monitoring-webserver      Moritoring Web server
monitoring-mysql          Moritoring mysql

```

Estos comandos permitirán arrancar y parar los servicios, entrar a administrar los containers que los exponen y monitorizar ambos.

## Contribuir

Se aceptan contribuciones en enrique.agrasar@edu.xunta.es